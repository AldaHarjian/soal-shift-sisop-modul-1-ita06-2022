# soal-shift-sisop-modul-1-ITA06-2022
Kelompok ITA06

1. Alda Risma Harjian (5027201004)
2. Richard Nicolas (5027201021)
3. Dzaki Indra Cahya (5027201053)

## Soal 1 (Revisi)
 1.	Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.<br>
a.	Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh <br>
b.	Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut <br>
i.	Minimal 8 karakter <br>
ii.	Memiliki minimal 1 huruf kapital dan 1 huruf kecil<br>
iii.	Alphanumeric<br>
iv.	Tidak boleh sama dengan username<br>
c.	Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.<br>
i.	Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists<br>
ii.	Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully<br>
iii.	Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME<br>
iv.	Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in<br>
d.	Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :<br>
i.	dl N ( N = Jumlah gambar yang akan didownload)<br>
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.<br>
ii.	att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.<br>

### 1a 
Disini diminta Untuk membuat file register.sh untuk pendaftaran user kemudian membuat file user.txt di folder users untuk menyimpan username dan password, dan file main.sh digunakan untuk login user yang telah terdaftar.

yang pertama menggunakan
```sh
#!/bin/bash

if [ ! -d log.txt ]; then
    touch log.txt
fi

if [ ! -d ./users ]; then
    mkdir ./users
fi

if [ ! -d ./users/user.txt ]; then
    touch ./users/user.txt
fi

time="$(date +'%D %H:%M:%S')"
```
-`-d` berfungsi untuk mengecek file/directory
- variable time berisikan tanggal,jam,menit dan detik agar mudah mengisikan laporan pada log.txt
melakukan pengecekan apakah file log.txt dan file users.txt sudah tebuat atau belum maka akan langsung dibuat.

### 1b
pada tahap register,Pasword yang telah terisi harus memiliki kriteria yang diminta,yaitu:i.minimal 8 karakter:
```sh
if [ ${#password} -lt 8 ] ; then
```
dari diatas sxpr legth password akan menghitung jumlah huruf pada password dan -lt 8: Less then berfungsi untuk mengecek apakah password kurang dari 8.
ii.memiliki minimal 1 huruf kapital dan 1 huruf kecil:
```sh
if [ $password = ${password^^} ] ; then 
   echo "Password minimal 1 huruf kecil" 
```
pada bagian disni passwordnya akan disamakan yang telah di jadikan huruf kappital semuanya.jikalau sama maka akan menampilkan pesan"password minimal 1 huruf kapital".
iii.Alphanumeric
```sh
if ! [[ $password =~ [0-9] ]] ; then 
   echo "Password harus ada angkanya"
elif
```
diatas ini password akan di cek menggunakan "=~", ini akan di cek apakah password tersebut tidak terdapat angka 0-9. jika tidak maka akan menampilkan pesan"password harus ada angkanya"

iv.tidak noleh sama password dan username
```sh
[ $username == $password ]; then 
     echo "Password tidak boleh sama dengan username"
     exit 1
```
di atas sini akan di cek pasword dan username sama atu tidak, jika sama akum muncul peesan"Password tidak boleh sama dengan username".

```sh
 echo "$username $password" >> ./users/user.txt && echo "$username berhasil ditambahkan" &&  echo "$time REGISTER: INFO User $username registered successfully" >> log.txt  
fi
```
setelah berhasil akan ada pesan $username berhasil ditambahkan dan username dan passwordnya akan disimpan di file user.txt, dan akan ada laporan bahwa ada user yang telah terdaftar pada log.txt

![Output result](img/1b.png)<br>
nanti akan terdaftar pada log dan mengeluarkan pesan yang ada seperti gambar tersebut.

### 1c
setiap percobaan login dan register akan di simpan ke file log.txt <br>
![Output result](img/1c.1.png)<br>

i.jika username yang sudah dimaksukan sudah ada yang terdaftar maka akan menampilkan pesan kalau user telah terdaftar dan itu akan otomatis masuk ke log.txt
```sh
if grep -q "$username" ./users/user.txt ; then
   echo "User $username sudah ada"
   echo "$time REGISTER: ERROR $username already exists" >> log.txt 
```
-`grep -q` berfungsi untuk mencari kata/kalimat yang ditunjuk pada sebuah file
![Output result](img/1c.2.png)<br>
![Output result](img/1c.2a.png)<br>

ii.ketika precobaan register berhasil maka akan ditambahkan laporan "username register successfully" pada log.txt
pada gambar diatas tadi.

iii. ketika user mencoba login dan password salah akan melaporkan pada user tersebut akan gagal login.
```sh
else
     echo "Login tidak berhasil"
     echo "$time LOGIN: ERROR Failed login attempt on user $username"  >> log.txt
     exit 1
 fi
 ```
 ![Output result](img/1c.2b.png)<br>

iv.ketika user berhasil login maka akan ada laporan ke log.txt bahwa user terbut berhasil login.
![Output result](img/1c.2c.png)<br>
![Output result](img/1c.2d.png)<br>
![Output result](img/1c.2e.png)<br>

### 1d
setelah berhasil login akan muncul 2 option yaitu 1.mendownload gambar dan 2.hitung jumlah login yang gagal dan berhasil.
```sh
folder=$(date +'%Y-%m-%d')_$username
```
- berisikan tahun,bulan,tanggal hari_username yang sedang login.

i. ertama diminta untuk masukan berapa jumlah gambar yang ingin di download dari link yang telah diberikan
gambar yang telah di download akan dimasukan ke sebuah foder yang bernama "tahun-bulan-tanggal hari_username", gambar yang telah di download pun namanya diganti menjadi PIC_xx dengan nomor yang berurutan sesuai dengan jumlah yang didownload.

```sh
if [ -e ${folder}.zip ] ;then
     unzip -P $password $folder.zip 

             for ((num=1; num<=sum; num=num+1))
        do  
            if [ $num -lt 10 ] ; then
                echo "PIC_0$num"
                wget https://loremflickr.com/320/240 -O PIC_S0$num.jpeg
                mv -n PIC_S0$num.jpeg ./$folder
            else
                echo "PIC_$num"
                wget https://loremflickr.com/320/240 -O PIC_S$num.jpeg 
                mv -n PIC_S$num.jpeg ./$folder
            fi
        done
        zip -rP $password $folder.zip $folder 
        rm -r $folder
    exit 1
    ```
- `-o `berfungsi untuk rename file
- `-e `berfungsi untuk mencari file/directory yang ditunjuk
- `-n` berfungsi untuk mencari file/directory yang di tujuh
- `zip` berfungsi untuk menunjuk folder zip yang di inginkan
- `-r` berfungsi untuk menzip file secara teratur
- `-p` untuk zip menambahkan password yang telah di berikan
- `mv` memindahkan file

selanjutnta jika ada file zip yang sama 
```sh
if [ -e ${folder}.zip ] ;then
     unzip -P $password $folder.zip 

             for ((num=1; num<=sum; num=num+1))
        do  
            if [ $num -lt 10 ] ; then
                echo "PIC_0$num"
                wget https://loremflickr.com/320/240 -O PIC_S0$num.jpeg
                mv -n PIC_S0$num.jpeg ./$folder
            else
                echo "PIC_$num"
                wget https://loremflickr.com/320/240 -O PIC_S$num.jpeg 
                mv -n PIC_S$num.jpeg ./$folder
            fi
        done
        zip -rP $password $folder.zip $folder 
        rm -r $folder
    exit 1
    ```
    sama seperti diatas tadi akan di cek terlebih dahulu apakah ada zip yang bernama sama jika ada akan di unzip il zip lalu akakn masuk perulangan for untuk mendownload gambarnya

ii.pada option kedua ini akan diminta untuk menghitung jumlah login yang berhasil dan gagal
```sh
 elif [ $option -eq 2 ] ; then
    printf "Total login yang berhasil: " 
    grep "LOGIN: INFO User $username logged in" log.txt | wc -l
        printf "\nTotal login yang tidak berhasil: "  
        grep "LOGIN: ERROR Failed login attempt on user $username" log.txt | wc -l
```
- `wc -1` Word Count berfungsi untuk menghitung kata yang muncul pada senuah file,-I unutk menghitung jumlah line.keduanya dikombinasikan dan fungsi grep digunakan untuk mencari laporan yang berhasil dan gagal login dengan username sama dengan yang sedang login pada file log.txt.
![Output result](img/1d.png)<br>
![Output result](img/1c.2e.png)<br>



# Soal 2
Pada soal nomor 2, terdapat file *log_website_daffainfo.log* yang diberikan untuk diolah dengan menggunakan script awk, dengan ketentuan:
<ol type="a">
    <li>Memuat folder bernama <i>forensic_log_website_daffainfo_log</i> </li>
    <li>Mencari <b>rata-rata request per jam</b> lalu memasukkan output-nya ke dalam file bernama <i>ratarata.txt</i> ke dalam folder 2a. </li>
    <li>Menampilkan <b>IP yang paling banyak</b> melakukan request ke server beserta <b>jumlah requestnya</b>. Lalu, memasukkan output-nya ke dalam file baru bernama <i>result.txt</i> ke dalam folder 2a. </li>
    <li>Mencari request yang menggunakan user-agent <b>curl</b>. Lalu, memasukkan output-nya ke dalam file <i>result.txt`</i>.</li>
    <li>Mencari IP Address yang menyerang pada <b>tanggal 22 Januari pukul 2 pagi</b>. Kemudian masukkan daftar IP tersebut kedalam file bernama <i>result.txt</i>.</li>
</ol>

Format File:
> **File ratarata.txt** <br>
> Rata-rata serangan adalah sebanyak *rata_rata* requests per jam <br>
> 
> **File result.txt** <br>
> IP yang paling banyak mengakses server adalah: *ip_address* sebanyak *jumlah_request* request.
>
> Ada *jumlah_req_curl* requests yang menggunakan curl sebagai user-agent
>
> *ip_address 1* <br>
> *ip_address 2* <br>
> *dst*
---
## Penyelesaian Nomor 2
Script dimulai dengan 
```sh
#!/bin/bash
``` 
syntax tersebut digunakan untuk memberi tahu kernel linux untuk mengeksekusi path yang disertakan dalam program (bash).

### 2a Pembuatan folder
Untuk penyelesaian pada soal nomor 2a, kelompok kami menggunakan syntax berikut:
```sh
mkdir -p forensic_log_website_daffainfo_log
```
- `mkdir` atau *make directory* merupakan syntax untuk pembuatan folder/directory, sedangkan `-p` merupakan kependekan dari `--parents` yang berfungsi untuk agar tidak terjadi error jika folder tersebut sudah ada
- `forensic_log_website_daffainfo_log` merupakan nama folder yang diminta pada soal

Berikut gambar ketika menjalankan command `mkdir -p` pada terminal linux:
![mkdir -p](img/2a.jpeg)

### 2b Rata-rata request per jam (revisi)
Untuk mendapatkan rata-rata request per jam, cara yang kami gunakan adalah membagi total data dengan perbedaan jam. Script yang kami gunakan:

```sh
awk ' END{print "Rata-rata serangan adalah sebanyak", (NR-1)/12, "requests per jam"}
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt
```
- Script awk tersebut berfungsi untuk melakukan pencetakan tulisan sesuai yang diminta pada soal. 
- Lalu selanjutnya, `NR-1` merupakan total data yang mengakses website daffainfo. <br>`NR` menandakan *number of records* atau jumlah baris yang ada pada file log. NR dikurangi sebanyak 1 karena NR pertama hanya berisi nama setiap kolom.
- Perbedaan jam sebanyak `12` sebagai pembagi untuk mencari nilai rata-rata didapatkan secara manual dengan penjelasan berikut:

File *log_website_daffainfo.log*
>**Data pertama:** (Baris kedua) <br>
>"45.146.165.37":"22/Jan/2022:00:11:10":"POST /cgi-bin/.%2e/.%2e/.%2e/.%2e/bin/sh HTTP/1.1":400:5633:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
>
>**Data terakhir:** <br>
> "45.146.165.37":"22/Jan/2022:12:00:00":"POST /vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php HTTP/1.1":404:5601:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"

Website daffainfo pertama kali diakses pada tanggal 22 Januari pukul 00:11:10, sedangkan website tersebut terakhir kali diakses pada tanggal 22 Januari pukul 12:00:00. Oleh karena itu, terdapat perbedaan sebanyak **12 jam**  dari pengaksesan pertama kali hingga pengaksesan terakhir.

- Hasil pembagian `NR-1` dengan `12` nantinya akan dimasukkan ke dalam file `ratarata.txt` yang berada pada folder **forensic_log_website_daffainfo_log**

#### Output ratarata.txt
Berikut screenshot output dari file ratarata.txt sebagai penyelesaian dari nomor 2b:
![Output result](img/ratarata.jpeg)

### 2c IP yang paling banyak muncul (revisi)
Untuk mencari IP yang paling banyak muncul, kami menggunakan script berikut:
```sh
awk -F: '
    {if (count[$1]++ >= max) max = count[$1]}
    END{
        for(i in count)
            if (max == count[i]) 
                print "IP yang paling banyak mengakses server adalah:", 
                    substr(i, 2, length(i) - 2), "sebanyak", count[i], "request."
    }
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
```
- Script awk dimulai dengan '-F'yang artinya sebelum loop pada file dimulai, ditentukan terlebih dahulu oleh *Field separator* yaitu ':'

Contoh penggunaan -F pada file  *log_website_daffainfo.log*
> **NR ke-2** <br>
> "45.146.165.37":"22/Jan/2022:00:11:10":"POST /cgi-bin/.%2e/.%2e/.%2e/.%2e/bin/sh HTTP/1.1":400:5633:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
>
>Hal ini berarti `$1` = "45.146.165.37" *atau* IP Address, `$2` = "22/Jan/2022 *atau* tanggal, dst.

- Selamjutnya pada bagian `if (count[$1]++ >= max) max = count[$1]`, disini kami membuat variabel array bernama `count` dengan array key kolom 1 (IP), serta variabel `max` yang akan menampung jumlah terbesar IP yang sering muncul. Setiap awk melakukan perulangan di setiap data dan menemukan IP yang sama, jadi nilai array akan bertambah. Jika awk menemukan `count` dari array IP yang berbeda dan memiliki jumlah yang sama atau lebih besar dari variabel `max`, maka nilai `max` akan berubah menjadi nilai `count` tersebut.
- Selanjutnya saat loop berakhir, terdapat `for(i in count)` yang berarti loop untuk setiap array `i` atau setiap IP dari variabel count. Di dalam loop ini, terdapat `if (max == count[i])` yang dimana, ketika awk menemukan nilai max dari setiap array, maka awk akan menjalankan script berikut:
```sh
print "IP yang paling banyak mengakses server adalah:", substr(i, 2, length(i) - 2), "sebanyak", count[i], "request."
```

- Selanjutnya penjelasan untuk `substr(i, 2, length(i) - 2)`:
    - `substr` yaitu merupakan salah satu command berfungsi untuk  memilih sebuah substring dari input. `substr` pada script yang ditujukan untuk menghilangkan tanda petik 2 yang terletak pada kolom pertama atau yang dilambangkan sebagai **i**. 
    - `substr` memiliki syntax `substr(s, a, b)` yang berarti mengembalikan sebanyak **b** huruf dari string **s**, dimulai pada posisi/huruf ke-**a**. 
    - kemudian pada bagian `length(i) - 2`, `length(i)` digunakan untuk mengembalikan panjang dari **i** (karena panjang dari setiap IP berbeda-beda), lalu `- 2` karena terdapat 2 petik yang ada pada **i** 

> Contohnya, ketika i = *"216.93.144.47"* lalu dilakukan `substr(i, 2, length(i) - 2)`, akan mengembalikan tulisan *216.93.144.47* 

- Semua hasil dari script awk tersebut akan diletakkan atau dimasukkan ke dalam file `result.txt` yang terletak di baris pertama di dalam folder *forensic_log_website_daffainfo_log*

### 2d Request oleh user-agent Curl
Untuk mencari jumlah request yang dilakukan oleh user-agent Curl, kami menggunakan script berikut:
```sh
awk -F: '
/curl/{++count}
END {print "Ada", count, "requests yang menggunakan curl sebagai user-agent"} 
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
```
-Pertama-tama, disinikami deklarasikan terlebih dahulu menggunakan '-F:'
- Selanjutnya, untuk mencari user agent yang bernama curl, kami menggunakan syntax '/curl/{++count}' yang dimana setiap baris yang berisikan "curl", variabel count akan diiterasikan sebanyak 1.
- Kemudian setelah itu, pada saat loop selesai atau berakhir, maka akan mencetak tulisan sesuai dengan format yang telah ditentukan lalu dimasukkan ke dalam file 'result.txt' terletak pada baris terakhir pada folder *forensic_log_website_daffainfo_log*

### 2e IP pada tanggal 22 Januari jam 2 pagi
Untuk mencari IP yang melakukan request pada tanggal 22 Januari jam 2 pagi, kami menggunakan script berikut:
```sh
awk -F: '
     {if ($3 == "02")
     printf "%s\n", $1}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
- Pertama-tama dimulai dengan proses awk yaitu membaca file log_website_daffainfo.log
- Selanjutnya dilanjutkan dengan '-F: ' yang dimana ini untuk memberi tahu bahwa *field separator* dari file log_website_daffainfo.log adalah `:`.
- Lalu dilanjutkan dengan '{if ($3 == "02")' yang berarti jika kolom ketiga sama dengan tanggal 22 maka akan melakukan 'printf "%s\n", $1}', yang tujuannya untuk memprint hasil dari kolom 1 (IP)
- Lalu yang terakhir, hasil awk tadi akan diletakkan ke dalam file 'result.txt' pada baris terakhir yang berada dalam folder *forensic_log_daffainfo_log*

#### Output result.txt
Berikut screenshot output dari file result.txt sebagai penyelesaian nomor 2c hingga 2e:
![Output result](img/result.jpeg)

## Soal 3
Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
<br>b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
<br>c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
<br>d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

#### Penyelesaian
#### a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. 

```bash
#!/bin/bash
output="/home/richard/log/metrics_$(date +"%Y%m%d%H%M%S").log"
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output

mem="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s",$2,$3,$4,$5,$6,$7}')"
swap="$(free -m | awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}')"
path="$(du -sh /home/richard | awk '{printf "%s,%s",$2,$1}')"

echo "$mem,$swap,$path" >> $output
chmod 700 $output
```
Program diatas adalah file minute_log.sh, jika dijalankan akan membentuk file `metrics_yyyymmddhhmmss.log` dengan yyyy untuk tahun, mm untuk bulan, dd untuk tanggal, hh untuk jam, mm untuk menit, dan ss untuk detik (sesuai dengan waktu program dijalankan).<br>
```bash
#!/bin/bash
output="/home/richard/log/metrics_$(date +"%Y%m%d%H%M%S").log"
```
Pertama, kami membentuk variabel `$output` sebagai file log yang akan terbentuk ketika dijalankan dan untuk tempat penyimpanannya adalah `/home/richard/log/` dan nama filenya adalah `metrics_yyyymmddhhmmss.log` karena ketika `$(date)`dijalankan akan menyimpan timestamp dan kemudian penulisan filenya sesuai aturan yang diberikan. 
```bash
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output
```
Selanjutnya kami menuliskan header `mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size` yang dimasukkan ke variabel `$output` dengan `echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"> $output`<br>
```bash
mem="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s",$2,$3,$4,$5,$6,$7}')"
swap="$(free -m | awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}')"
path="$(du -sh /home/richard | awk '{printf "%s,%s",$2,$1}')"
```
Kemudian kami membentuk variabel `$mem, $swap, $path` yang fungsinya untuk menyimpan nilai dari mem (mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available), swap (swap_total, swap_used, swap_free), dan path (path, path_size) 

Untuk mem dan swap, kami menjalankan command `free -m` untuk monitoring ram dan kemudian command awk yaitu ` awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s",$2,$3,$4,$5,$6,$7}'` untuk menampilkan nilai yang mengandung `Mem:` dan command ` awk '/Swap:/ {printf "%s,%s,%s",$2,$3,$4}'` untuk menampilkan nilai yang mengandung `Swap:` 

Untuk path, kami menjalankan command `du -sh /home/richard` dan hasilnya adalah `path_size path` kemudian command `awk '{printf "%s,%s",$2,$1}'` untuk membalikkan hasilnya
```bash
echo "$mem,$swap,$path" >> $output
```
Langkah terakhir adalah menambahkan hasil dari variabel `$mem, $swap, $path` ke variabel `$output` yang sebelumnya telah terisi header, caranya adalah `echo "$mem,$swap,$path" >> $output` <br><br>
<b>Output dari file `minute_log.sh` (`metrics_yyyymmddhhmmss`) : </b>
![Output result](img/output_minute.png) <br>
#### b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
Untuk menyelesaikan soal ini, kami menggunakan cron job yang dijalankan di ubuntu masing-masing. Pertama-tama kami melakukan command `crontab -e` untuk mengedit cron job yang ada, kemudian kami mengetikkan `* * * * * /bin/bash /home/richard/soal-sift-sisop-modul-1-ita06-2022/soal3/minute.log.sh` seperti yang terlihat dibawah ini :
![Output result](img/cron.png) <br>
Hasil dari cronjob per menit adalah sebagai berikut : (terlihat ada file baru setiap menitnya)
![Output result](img/cron_res.png) <br>
Selain itu, kami juga menambahkan cron job untuk file `aggregate_minutes_to_hourly_log.sh` dengan mengetikkan `59 * * * * /bin/bash /home/richard/soal-sift-sisop-modul-1-ita06-2022/soal3/aggregate_minutes_to_hourly_log.sh` sehingga file akan dijalankan setiap jam pada menit ke-59 (menit terakhir setiap jamnya) dan hasilnya adalah mengenerate file metrics seperti aturan pada nomor 3c
#### c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. (revisi)
```bash
#!/bin/bash
output="/home/richard/log/metrics_$(date +"%Y%m%d%H").log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output

function list(){
    for file in $(ls /home/richard/log/metrics_* | grep $(date +"%Y%m%d%H"));
    do cat $file | grep -v mem;
    done
}

mt_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$1
    };
    if($1>max){
        max=$1
    };
    if($1<max){
        min=$1
    };
    sum+=$1; n+=1
    } END {
        print min,max,sum/n
    }')
mt_min=$(echo $mt_sort | awk '{print $1}' | tr ',' '.')
mt_max=$(echo $mt_sort | awk '{print $2}' | tr ',' '.')
mt_avg=$(echo $mt_sort | awk '{print $3}' | tr ',' '.')

mu_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$2
    };
    if($2>max){
        max=$2
    };
    if($2<max){
        min=$2
    };
    sum+=$2; n+=1
    } END {
        print min,max,sum/n
    }')
mu_min=$(echo $mu_sort | awk '{print $1}' | tr ',' '.')
mu_max=$(echo $mu_sort | awk '{print $2}' | tr ',' '.')
mu_avg=$(echo $mu_sort | awk '{print $3}' | tr ',' '.')

mf_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$3
    };
    if($3>max){
        max=$3
    };
    if($3<max){
        min=$3
    };
    sum+=$3; n+=1
    } END {
        print min,max,sum/n
    }')
mf_min=$(echo $mf_sort | awk '{print $1}' | tr ',' '.')
mf_max=$(echo $mf_sort | awk '{print $2}' | tr ',' '.')
mf_avg=$(echo $mf_sort | awk '{print $3}' | tr ',' '.')

ms_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$4
    };
    if($4>max){
        max=$4
    };
    if($4<max){
        min=$4
    };
    sum+=$4; n+=1
    } END {
        print min,max,sum/n
    }')
ms_min=$(echo $ms_sort | awk '{print $1}' | tr ',' '.')
ms_max=$(echo $ms_sort | awk '{print $2}' | tr ',' '.')
ms_avg=$(echo $ms_sort | awk '{print $3}' | tr ',' '.')

mb_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$5
    };
    if($5>max){
        max=$5
    };
    if($5<max){
        min=$5
    };
    sum+=$5; n+=1
    } END {
        print min,max,sum/n
    }')
mb_min=$(echo $mb_sort | awk '{print $1}' | tr ',' '.')
mb_max=$(echo $mb_sort | awk '{print $2}' | tr ',' '.')
mb_avg=$(echo $mb_sort | awk '{print $3}' | tr ',' '.')

ma_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$6
    };
    if($6>max){
        max=$6
    };
    if($6<max){
        min=$6
    };
    sum+=$6; n+=1
    } END {
        print min,max,sum/n
    }')
ma_min=$(echo $ma_sort | awk '{print $1}' | tr ',' '.')
ma_max=$(echo $ma_sort | awk '{print $2}' | tr ',' '.')
ma_avg=$(echo $ma_sort | awk '{print $3}' | tr ',' '.')

st_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$7
    };
    if($7>max){
        max=$7
    };
    if($7<max){
        min=$7
    };
    sum+=$7; n+=1
    } END {
        print min,max,sum/n
    }')
st_min=$(echo $st_sort | awk '{print $1}' | tr ',' '.')
st_max=$(echo $st_sort | awk '{print $2}' | tr ',' '.')
st_avg=$(echo $st_sort | awk '{print $3}' | tr ',' '.')

su_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$8
    };
    if($8>max){
        max=$8
    };
    if($8<max){
        min=$8
    };
    sum+=$8; n+=1
    } END {
        print min,max,sum/n
    }')
su_min=$(echo $su_sort | awk '{print $1}' | tr ',' '.')
su_max=$(echo $su_sort | awk '{print $2}' | tr ',' '.')
su_avg=$(echo $su_sort | awk '{print $3}' | tr ',' '.')

sf_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$9
    };
    if($9>max){
        max=$9
    };
    if($9<max){
        min=$9
    };
    sum+=$9; n+=1
    } END {
        print min,max,sum/n
    }')
sf_min=$(echo $sf_sort | awk '{print $1}' | tr ',' '.')
sf_max=$(echo $sf_sort | awk '{print $2}' | tr ',' '.')
sf_avg=$(echo $sf_sort | awk '{print $3}' | tr ',' '.')

ps_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$11
    };
    if($11>max){
        max=$11
    };
    if($11<max){
        min=$11
    };
    sum+=$11; n+=1
    } END {
        print min,max,sum/n
    }')
ps_min=$(echo $ps_sort | awk '{print $1}' | tr ',' '.')
ps_max=$(echo $ps_sort | awk '{print $2}' | tr ',' '.')
ps_avg=$(echo $ps_sort | awk '{print $3}' | tr ',' '.')


path="/home/richard"

echo "minimum,$mt_min,$mu_min,$mf_min,$ms_min,$mb_min,$ma_min,$st_min,$su_min,$sf_min,$path,$ps_min" >> $output
echo "maximum,$mt_max,$mu_max,$mf_max,$ms_max,$mb_max,$ma_max,$st_max,$su_max,$sf_max,$path,$ps_max" >> $output
echo "average,$mt_avg,$mu_avg,$mf_avg,$ms_avg,$mb_avg,$ma_avg,$st_avg,$su_avg,$sf_avg,$path,$ps_avg" >> $output

chmod 700 $output
```
Program diatas adalah file aggregate_minutes_to_hourly_log.sh, jika dijalankan akan membentuk file `metrics_yyyymmddhh.log` dengan yyyy untuk tahun, mm untuk bulan, dd untuk tanggal, dan hh untuk jam (sesuai dengan waktu program dijalankan).<br>
```bash
#!/bin/bash
output="/home/richard/log/metrics_$(date +"%Y%m%d%H").log"
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $output
```
Pada bagian pertama, kami mendeklarasikan variabel `$output` dan header yang fungsinya sama dengan yang sebelumnya (3a).
```bash
function list(){
    for file in $(ls /home/richard/log/metrics_* | grep $(date +"%Y%m%d%H"));
    do cat $file | grep -v mem;
    done
}
```
Selanjutnya, kami membuat fungsi list untuk membaca semua file metrics yang tergenerate setiap menitnya dan diambil nilainya
```bash
mt_sort=$(list | awk -F, '{
    if(min==""){
        min=max=$1
    };
    if($1>max){
        max=$1
    };
    if($1<max){
        min=$1
    };
    sum+=$1; n+=1
    } END {
        print min,max,sum/n
    }')
mt_min=$(echo $mt_sort | awk '{print $1}' | tr ',' '.')
mt_max=$(echo $mt_sort | awk '{print $2}' | tr ',' '.')
mt_avg=$(echo $mt_sort | awk '{print $3}' | tr ',' '.')
```
Langkah selanjutnya yang kami lakukan adalah membuat sebuah variabel `mt_sort` (mem total) yang merupakan pemilah semua data dari fungsi sebelumnya (list), dengan menggunakan awk, kami menentukan nilai max, min, dan menghitung rata-rata dari semua file yang terbaca.<br>
Awalnya, kami mengecek variabel min, jika kosong maka variabel min dan max akan diisi oleh nilai `$1` dari file pertama di list kemudian dicek apakah nilai tersebut lebih besar atau lebih kecil dari nilai max dan min. jika memenuhi kondisi (lebih besar dari max atau lebih kecil dari min) maka variabel max atau min akan dioverwrite dengan nilai yang baru. <br>
Untuk nilai rata-rata, semua file akan ditambahkan ke variabel sum dan untuk setiap perulangannya n akan bertambah satu. <br>
Pada saat loop berakhir, maka nilai `min`, `max`, dan hasil dari `sum/n` (rata-rata = sum dibagi n) akan dituliskan. Nilai-nilai tersebut kemudian akan dimasukkan ke variabel `mt_min`,`mt_max`, dan `mt_avg` sesuai dengan urutannya, $1 $2 $3 <br><br>
Untuk mem dan swap selanjutnya sama dengan langkah diatas, yang berbeda adalah nilai yang digunakan, mem total menggunakan `$1`, mem used menggunakan `$2`, mem free menggunakan `$3`, dst.<br>
```bash
path="/home/richard"

echo "minimum,$mt_min,$mu_min,$mf_min,$ms_min,$mb_min,$ma_min,$st_min,$su_min,$sf_min,$path,$ps_min" >> $output
echo "maximum,$mt_max,$mu_max,$mf_max,$ms_max,$mb_max,$ma_max,$st_max,$su_max,$sf_max,$path,$ps_max" >> $output
echo "average,$mt_avg,$mu_avg,$mf_avg,$ms_avg,$mb_avg,$ma_avg,$st_avg,$su_avg,$sf_avg,$path,$ps_avg" >> $output
```
Untuk path sendiri tidak menggunakan perulangan tetapi dideklarasikan secara tersendiri karena disetiap filenya path selalu sama yaitu `home/richard`. Setelah itu, diprint data yang telah didapat sesuai dengan urutannya (min, max, avg) dan dimasukkan ke variabel `$output`<br><br>
<b>Output dari file `aggregate_minutes_to_hourly_log.sh` (`metrics_yyyymmddhh`) :</b>
![Output result](img/output_hour.png) <br>
#### d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file. (revisi)
Dikarenakan diakhir setiap file terdapat `chmod 700 $output` maka file metrics yang terbentuk hanya bisa di-read, write, dan execute oleh pemiliknya <br>
![Output result](img/cron_res.png) <br>
Gambar diatas menampilkan detail file yang terbentuk di folder log, terlihat file yang tergenerate oleh file `minute.log.sh` dan `aggregate_minutes_to_hourly_log.sh` akan memiliki permission `-rwx------`
